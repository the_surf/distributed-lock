package com.fun.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fun.entity.Lock;
import com.fun.jedis.JedisService;

/**
 * ClassName: LockUtil <br/>
 * date: 2018年9月2日 下午12:26:13 <br/>
 * 
 * @author lambert
 * @version
 * @since JDK 1.8
 */
@Component
public class LockUtil {

	/**
	 * 锁超时时间:s(防止死锁)
	 */
	private static final int LOCK_TIMEOUT = 30;

	/**
	 * 获取锁的时间间隔:ms
	 */
	private static final long REQUIRE_LOCK = 30L;

	/**
	 * 尝试获取锁的时间:ms
	 */
	private static final long REQUIRE_TRY = 100L;

	@Autowired
	JedisService jedisService;

	/**
	 * 
	 * requireLock:(获取锁). <br/>
	 * 
	 * @author lambert
	 * @param lock
	 * @return
	 * @since JDK 1.8
	 */
	public boolean requireLock(Lock lock) {
		return getLock(lock, LOCK_TIMEOUT, REQUIRE_LOCK, REQUIRE_TRY);
	}

	/**
	 * 
	 * requireLock:(获取锁，获取时间间隔). <br/>
	 * 
	 * @author lambert
	 * @param lock
	 * @param requireLock
	 * @return
	 * @since JDK 1.8
	 */
	public boolean requireLock(Lock lock, long requireLock) {
		return getLock(lock, LOCK_TIMEOUT, requireLock, REQUIRE_TRY);
	}

	/**
	 * 
	 * requireLock:(获取锁，锁失效时间，获取时间间隔). <br/>
	 * 
	 * @author lambert
	 * @param lock
	 * @param lockTimeOut
	 * @param requireLock
	 * @return
	 * @since JDK 1.8
	 */
	public boolean requireLock(Lock lock, int lockTimeOut, long requireLock) {
		return getLock(lock, lockTimeOut, requireLock, REQUIRE_TRY);
	}

	/**
	 * 
	 * requireLock:(获取锁，锁失效时间，获取时间间隔,获取锁持续时间). <br/>
	 * 
	 * @author lambert
	 * @param lock
	 * @param lockTimeOut
	 * @param requireLock
	 * @param requireTry
	 * @return
	 * @since JDK 1.8
	 */
	public boolean requireLock(Lock lock, int lockTimeOut, long requireLock, long requireTry) {
		return getLock(lock, lockTimeOut, requireLock, REQUIRE_TRY);
	}

	/**
	 * 
	 * shutLock:(释放锁). <br/>
	 * 
	 * @author lishuai11
	 * @param lock
	 * @since JDK 1.8
	 */
	public void shutLock(Lock lock) {
		jedisService.del(lock.getKey());
	}

	private boolean getLock(Lock lock, int lockTimeOut, long requireLock, long requireTry) {
		try {
			if (StringUtils.isEmpty(lock.getKey()) || StringUtils.isEmpty(lock.getValue())) {
				return false;
			}
			long startTime = System.currentTimeMillis();
			do {
				if (!jedisService.isExist(lock.getKey())) {
					jedisService.setex(lock.getKey(), lockTimeOut, lock.getValue());
					return true;
				} else {
					if (System.currentTimeMillis() - startTime > requireTry) {
						return false;
					}
					Thread.sleep(requireLock);
				}
			} while (jedisService.isExist(lock.getKey()));
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

}
