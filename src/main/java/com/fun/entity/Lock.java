package com.fun.entity;

/**
 * ClassName: Lock <br/>
 * date: 2018年9月2日 下午12:24:13 <br/>
 * 
 * @author lambert
 * @version
 * @since JDK 1.8
 */
public class Lock {

	private String key;

	private String value;

	public Lock(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
