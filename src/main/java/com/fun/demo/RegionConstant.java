package com.fun.demo;

/**
 * ClassName: ShowAuditConstant
 * Function: TODO(请描述该类的功能)
 * date: 2019-05-02 15:09
 *
 * @author lambert
 * @since JDK 1.8
 */
public enum RegionConstant {

    LABEL_NULL(0, "空"),
    LABEL_EU_AM(1, "欧美"),
    LABEL_MID_EAST(2, "中东"),
    LABEL_INDIA(3, "印度"),
    LABEL_AU(4, "南半球"),
    LABEL_REGION_SIZE(4, "地区大小"),

    SHOW_EU_AM(1, "欧美"),
    SHOW_MID_EAST(2, "中东"),
    SHOW_INDIA(3, "印度");


    private Integer code;

    private String region;

    RegionConstant(Integer code, String region) {
        this.code = code;
        this.region = region;
    }

    public Integer getCode() {
        return this.code;
    }

    public String getRegion() {
        return this.region;
    }
}
