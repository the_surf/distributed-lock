package com.fun.demo;

/**
 * ClassName: ImageConstant
 * Function: TODO(请描述该类的功能)
 * date: 2019-05-13 15:17
 *
 * @author lambert
 * @since JDK 1.8
 */
public class ImageConstant {

    public static final String MIDDLE_TAIL = "_thumbnail_450x450";

    public static final String SMALL_TAIL = "_thumbnail_350x350";

    public static final Integer IMAGE_TYPE_SHOW = 2;
}
