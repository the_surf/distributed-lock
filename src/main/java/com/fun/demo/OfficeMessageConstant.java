package com.fun.demo;

/**
 * ClassName: OfficeMessageConstant
 * Function: TODO(请描述该类的功能)
 * date: 2019-05-17 10:20
 *
 * @author lambert
 * @since JDK 1.8
 */
public enum OfficeMessageConstant {

    TYPE_SELECT(7, "精选"),
    TYPE_WINNER(8, "WINNER"),

    IMG_URL_SIZE(0, "small");

    private Integer value;

    private String message;

    OfficeMessageConstant(Integer value, String message) {
        this.value = value;
        this.message = message;
    }

    public Integer getValue() {
        return this.value;
    }

    public String getMessage() {
        return this.message;
    }

    public static class RedisHashTable {
        public static final String USER_MSG_COUNT_OFFICIAL = "user_msg_count_official";
        public static final String LAST_OUTFIT_OFFICIAL_INFO = "last_outfit_official_info";

    }
}
