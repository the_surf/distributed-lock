package com.fun.demo;

/**
 * ClassName: OfficeMessageConstant
 * Function: TODO(请描述该类的功能)
 * date: 2019-05-17 10:20
 *
 * @author lambert
 * @since JDK 1.8
 */
public enum PointConstant {

    SHOW_CREAT_POINT(5, "show发布5分"),

    SHOW_SELECT_POINT(50, "show精选50分");

    private Integer value;

    private String message;

    PointConstant(Integer value, String message) {
        this.value = value;
        this.message = message;
    }

    public Integer getValue() {
        return this.value;
    }

    public String getMessage() {
        return this.message;
    }

    /**
     * 积分风模块类型
     */
    public static class ModelType {

        public static final Integer SHOW_CREAT = 1;

        public static final Integer SHOW_SELECT = 2;

    }

    /**
     * 南京添加积分接口，申请指定类型
     */
    public static class PointType {

        public static final Integer SHOW_CREAT = 37;

        public static final Integer SHOW_SELECT = 38;

    }

    /**
     * 积分风控上限
     */
    public static class LessThan {

        public static final Integer SHOW_CREAT_TOP = 15;

        public static final Integer SHOW_SELECT_TOP = 5000;
    }

    /**
     * 积分方法返回info
     */
    public static class ResponseInfo {

        public static final String SHOW_SELECT_POINT_THAN_5000 = "积分超过5000，不可被精选";

        public static final String SHOW_SELECT_POINT_SUCCESS = "添加积分成功";

        public static final String SHOW_SELECT_POINT_API_ERROR = "添加积分失败，无法被精选，请重试";

    }

}
