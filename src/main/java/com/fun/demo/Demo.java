package com.fun.demo;

import com.fun.util.LockUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ClassName: Demo <br/>
 * date: 2018年9月2日 下午1:16:00 <br/>
 * 
 * @author lambert
 * @version
 * @since JDK 1.8
 */
@RestController
@RequestMapping("/test/")
public class Demo {

	@Autowired
	LockUtil lockUtil;

	@RequestMapping("go")
	public String test() {
		String a =  "ccc";
		return "";
	}
}
